# phsd2root
Converter from the 'phsd.dat' format to [ROOT](https://root.cern/) files.

## Authors
* **Viktar Kireyeu** - *Initial work*


## Usage
Compile:  
```
make
```

Run:  
```
./convert_phsd inputPHSD phsd.dat.gz output.root  
```

Draw Px, pseudorapidity and rapidity of kaons, protons, neutrons:  
```
PHSDtree->Draw("event.fParticles.fPx", "event.fParticles.fPDG==321")  
PHSDtree->Draw("event.fParticles->Eta()", "event.fParticles.fPDG==2212")  
PHSDtree->Draw("event.fParticles->Rapidity()", "event.fParticles.fPDG==2112")  
```  
  
