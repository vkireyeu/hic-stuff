#include <algorithm>

#include <TObject.h>
#include <TClonesArray.h>

#include "libPHSDEvent.h"

Particle::Particle(){
  fId = fPDG = fCharge = fChannel = 0;
  fPx = fPy = fPz = fE = 0.;
}

Particle::~Particle(){  // Destructor
}


Event::Event(){
  fB = 0.;
  fNpart = 0;
  fNparticipants = 0;
  std::fill(fPsi, fPsi+4, 0);
  std::fill(fEcc, fEcc+4, 0);
  fParticles = new TClonesArray("Particle", 100);
}

Event::~Event(){
  Clear();
  delete fParticles;
}

Float_t Event::GetPsi(Int_t nth){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
    return -1;
  }
  return fPsi[nth-2];
}

Float_t Event::GetEcc(Int_t nth){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
    return -1;
  }
  return fEcc[nth-2];
}

void Event::SetPsi(Int_t nth, Float_t val){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
  }
  else fPsi[nth-2] = val;
}

void Event::SetEcc(Int_t nth, Float_t val){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
  }
  else fEcc[nth-2] = val;
}


Particle* Event::GetParticle(Int_t index) const{
  if(index < 0) {
    return NULL;
  }
  if(index >= fNpart) {
    return NULL;
  }
  return ((Particle*) fParticles->At(index));
}

void Event::AddParticle(const Particle& particle)
{
  new ((*fParticles)[fNpart]) Particle(particle);
  fNpart += 1;
}

void Event::Clear(){
  fParticles->Clear();
  fB = 0.;
  fNpart = 0;
  fNparticipants = 0;
  std::fill(fPsi, fPsi+4, 0);
  std::fill(fEcc, fEcc+4, 0);
}





DataHeader::DataHeader(){
  fAtar  = fZtar = fAproj = fZproj = 0;
  fTkin = fBmin = fBmax = fDeltaB = fFinalT = 0.;
  fIGLUE = fIBW = fNUM = fSUB = 0;
  fIdil = fICQ = fIHARD = 0;
}

DataHeader::~DataHeader(){  // Destructor
}


void DataHeader::Print(){
  printf("Mode: %s\n", fMode.Data());
  printf("Target(A,Z): %4d, %4d\n", fAtar, fZtar);
  printf("Projectile(A,Z): %4d, %4d\n", fAproj, fZproj);
  printf("Tkin: %8.2f, Plab: %8.2f, Sqrt(S): %8.2f\n", fTkin, this->GetPlab(), this->GetSRT());
  printf("Bmin: %4.2f, Bmax: %4.2f, DeltaB: %4.2f, IBweight_MC=%1d\n", fBmin, fBmax, fDeltaB, fIBW);
  printf("NUM: %4d, SUB: %4d\n", fNUM, fSUB);
  printf("FinalT: %4.2f\n", fFinalT);
  printf("IGLUE=%1d, Idilept=%1d, ICQ=%1d, IHARD=%1d\n", fIGLUE, fIdil, fICQ, fIHARD);
}


ClassImp(Event);
ClassImp(Particle);
ClassImp(DataHeader);

