#include "libPHSDEvent.h"
#include "utils.h"

int main(int argc, char* argv[]){
  if(argc < 4){
    printf("usage: %s <inputPHSD> <phsd.dat.gz> <out.root>\n", argv[0]);
    return 1;
  }
  else{
    inputPHSD = argv[1];
    filename  = argv[2];
  }

  TFile *OUTfile = new TFile(argv[3], "RECREATE");
//  OUTfile -> SetCompressionAlgorithm(ROOT::kZLIB);
  OUTfile -> SetCompressionLevel(6);

  DataHeader *header = new DataHeader();
// --- inputPHSD file reading
  Int_t Atar, Ztar, Aproj, Zproj;
  Float_t Tkin, Bmin, Bmax, DeltaB;
  Int_t NUM, SUB;
  Int_t IGLUE;
  Float_t FinalT;
  Int_t Idil, ICQ, IHARD, IBwMC;

  fgzFile = gzopen(inputPHSD, "rb");
  if(!fgzFile){
    printf("-E- Can not open file: %s\n", inputPHSD);
    exit(1);
  }

  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Atar);   header -> SetAtar(Atar);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Ztar);   header -> SetZtar(Ztar);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Aproj);  header -> SetAproj(Aproj);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Zproj);  header -> SetZproj(Zproj);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Tkin);   header -> SetTkin(Tkin);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Bmin);   header -> SetBmin(Bmin);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Bmax);   header -> SetBmax(Bmax);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &DeltaB); header -> SetDeltaB(DeltaB);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &NUM);    header -> SetNum(NUM);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &SUB);    header -> SetSub(SUB);
  gzgets(fgzFile, fbuffer, 256); // ISEED
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IGLUE);  header -> SetIGLUE(IGLUE);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &FinalT); header -> SetFinalT(FinalT);
  gzgets(fgzFile, fbuffer, 256); // ILOW
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Idil);   header -> SetIdil(Idil);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &ICQ);    header -> SetICQ(ICQ);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IHARD);  header -> SetIHARD(IHARD);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IBwMC);  header -> SetIBW(IBwMC);
  gzclose(fgzFile);

  IGLUE==1 ? header -> SetMode("PHSD") : header -> SetMode("HSD");
  header -> Write();
  header -> Print();


  TTree *PHSDtree = new TTree("PHSDtree", "PHSD tree");
  Event *event = new Event();
  PHSDtree -> Branch("event", "Event", &event);

// --- ASCII files stuff
  int    pBparts, pCharge, pChannel;
  double pB, pPx, pPy, pPz, pE;
  long   PDG;
  int    EventNr = 0;
  int    Npart;
  double psi2, psi3, psi4, psi5;
  double ecc2, ecc3, ecc4, ecc5;
// ASCII - read + calculate
//S----------------------------------------------------------------

  fgzFile = gzopen(filename, "rb");
  if(!fgzFile){
    printf("-E- Can not open file: %s\n", filename);
    exit(1);
  }

  while (gzgets(fgzFile, fbuffer, 256) != NULL){
    if (gzeof(fgzFile)){
      gzclose(fgzFile);
      fgzFile=NULL;
      break;
    }

    sscanf(fbuffer, "%d %*d %*d %lf", &pBparts, &pB);
    gzgets(fgzFile, fbuffer, 256);       // 2nd line
    sscanf(fbuffer, "%d %lf %lf %lf %lf %lf %lf %lf %lf", &Npart, &psi2, &ecc2, &psi3, &ecc3, &psi4, &ecc4, &psi5, &ecc5);
// --- loop over particles
    for (int i = 0; i < pBparts; ++i){
      gzgets(fgzFile, fbuffer, 256);
      sscanf(fbuffer, "%ld %d %lf %lf %lf %lf %d", &PDG, &pCharge, &pPx, &pPy, &pPz, &pE, &pChannel);

      Particle *particle = new Particle();
      particle -> SetId(i);
      particle -> SetPdg(PDG);
      particle -> SetCharge(pCharge);
      particle -> SetPx(pPx);
      particle -> SetPy(pPy);
      particle -> SetPz(pPz);
      particle -> SetE(pE);
      particle -> SetChannel(pChannel);

      event -> AddParticle(*particle);
    }// end of the particles (baryons or phsd.dat) loop
    event -> SetB(pB);
    event -> SetNpart(pBparts);
    event -> SetNparticipants(Npart);
    event -> SetPsi(2, psi2);
    event -> SetPsi(3, psi3);
    event -> SetPsi(4, psi4);
    event -> SetPsi(5, psi5);
    event -> SetEcc(2, ecc2);
    event -> SetEcc(3, ecc3);
    event -> SetEcc(4, ecc4);
    event -> SetEcc(5, ecc5);

    PHSDtree -> Fill();
    event -> Clear();
    ++EventNr;
  }
  gzclose(fgzFile);
//E----------------------------------------------------------------

  OUTfile -> Write();
  OUTfile -> Close();

  return 0;
}
