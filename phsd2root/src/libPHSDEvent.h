#ifndef PHSDEVENT
#define PHSDEVENT

#include <TClonesArray.h>
#include <TMath.h>
#include <TObject.h>
#include <TNamed.h>

class Particle : public TObject {

 private:
  Int_t        fId;
  Int_t       fPDG;
  Int_t    fCharge;
  Int_t   fChannel;
  Float_t      fPx;
  Float_t      fPy;
  Float_t      fPz;
  Float_t       fE;

 public:
  Particle();
  virtual ~Particle();

  inline Int_t   GetId()      const {return fId;}
  inline Int_t   GetPdg()     const {return fPDG;}
  inline Int_t   GetCharge()  const {return fCharge;}
  inline Int_t   GetChannel() const {return fChannel;}
  inline Float_t Px()         const {return fPx;}
  inline Float_t Py()         const {return fPy;}
  inline Float_t Pz()         const {return fPz;}
  inline Float_t Pt()         const {return TMath::Sqrt(fPx*fPx + fPy*fPy);}
  inline Float_t P()          const {return TMath::Sqrt(fPx*fPx + fPy*fPy + fPz*fPz);}
  inline Float_t E()          const {return fE;}
  inline Float_t Phi()        const {return TMath::ATan2(fPy, fPx);}
  inline Float_t Rapidity()   const {return 0.5 * TMath::Log((fE+fPz)/(fE-fPz));}
  inline Float_t Eta()        const {return 0.5 * TMath::Log((P() + fPz) / (P() - fPz));}

  inline void SetId      (Int_t id)        {fId = id;}
  inline void SetPdg     (Int_t pdg)     {fPDG = pdg;}
  inline void SetCharge  (Int_t q)      {fCharge = q;}
  inline void SetChannel (Int_t ch)   {fChannel = ch;}
  inline void SetPx      (Float_t px)      {fPx = px;}
  inline void SetPy      (Float_t py)      {fPy = py;}
  inline void SetPz      (Float_t pz)      {fPz = pz;}
  inline void SetE       (Float_t  e)        {fE = e;}

  ClassDef(Particle, 1);
};



class Event : public TObject {
 private:
  Double_t	fB;
  Int_t         fNpart;         // Number of particles in event
  Int_t         fNparticipants; // Number of participants in event
  Float_t	fPsi[4];        // Participant plane angle of the n-th harmonic: 2,3,4,5
  Float_t	fEcc[4];        // Eccentricity of the n-th harmonic: 2,3,4,5
  TClonesArray* fParticles;

 public:
  Event();
  virtual ~Event();
  inline Double_t GetB()             const {return fB;}
  inline Int_t    GetNpart()         const {return fNpart;}
  inline Int_t    GetNparticipants() const {return fNparticipants;}
  Float_t GetPsi(Int_t nth);
  Float_t GetEcc(Int_t nth);

  inline TClonesArray* GetParticleList() const {return fParticles;}
  Particle* GetParticle(Int_t index) const;

  inline void SetB	       (Double_t b)  {fB = b;}
  inline void SetNpart         (Int_t Npart) {fNpart = Npart;}
  inline void SetNparticipants (Int_t Np)    {fNparticipants = Np;}
  void SetPsi(Int_t nth, Float_t val);
  void SetEcc(Int_t nth, Float_t val);

  void AddParticle(const Particle& particle);
  void Clear();

  ClassDef(Event, 1);
};



class DataHeader : public TObject {
 private:
  Int_t    fAtar;
  Int_t    fZtar;
  Int_t   fAproj;
  Int_t   fZproj;
  Float_t  fTkin;
  TString  fMode;
  Float_t  fBmin;
  Float_t  fBmax;
  Float_t  fDeltaB;
  Int_t    fIGLUE;
  Int_t    fIBW;
  Int_t    fNUM;
  Int_t    fSUB;
  Float_t  fFinalT;
  Int_t    fIdil;
  Int_t    fICQ;
  Int_t    fIHARD;

 public:
  DataHeader();
  virtual ~DataHeader();
  inline Int_t     GetAtar() const {return  fAtar;}
  inline Int_t     GetZtar() const {return  fZtar;}
  inline Int_t    GetAproj() const {return fAproj;}
  inline Int_t    GetZproj() const {return fZproj;}
  inline Float_t   GetTkin() const {return  fTkin;}
  inline Float_t   GetPlab() const {return TMath::Sqrt(fTkin*fTkin + 2.*0.938*fTkin);}
  inline Float_t    GetSRT() const {return TMath::Sqrt(4.*(0.938*0.938) + 2.*0.938*fTkin);}
  inline TString   GetMode() const {return fMode;}
  inline Float_t   GetBmin() const {return  fBmin;}
  inline Float_t   GetBmax() const {return  fBmax;}
  inline Float_t GetDeltaB() const {return  fDeltaB;}
  inline Int_t    GetIGlue() const {return  fIGLUE;}
  inline Int_t      GetIBW() const {return  fIBW;}
  inline Int_t      GetNum() const {return  fNUM;}
  inline Int_t      GetSub() const {return  fSUB;}
  inline Float_t GetFinalT() const {return  fFinalT;}
  inline Int_t     GetIdil() const {return  fIdil;}
  inline Int_t      GetICQ() const {return  fICQ;}
  inline Int_t    GetIHARD() const {return  fIHARD;}

  inline void  SetAtar(Int_t  Atar)   {fAtar = Atar;}
  inline void  SetZtar(Int_t  Ztar)   {fZtar = Ztar;}
  inline void SetAproj(Int_t Aproj) {fAproj = Aproj;}
  inline void SetZproj(Int_t Zproj) {fZproj = Zproj;}
  inline void SetTkin(Float_t Tkin)   {fTkin = Tkin;}
  inline void SetMode(TString Mode)   {fMode = Mode;}
  inline void SetBmin(Float_t Bmin)   {fBmin = Bmin;}
  inline void SetBmax(Float_t Bmax)   {fBmax = Bmax;}
  inline void SetDeltaB(Float_t dB)   {fDeltaB = dB;}
  inline void  SetIGLUE(Int_t  igl)   {fIGLUE = igl;}
  inline void    SetIBW(Int_t  ibw)     {fIBW = ibw;}
  inline void    SetNum(Int_t  num)     {fNUM = num;}
  inline void    SetSub(Int_t  sub)     {fSUB = sub;}
  inline void SetFinalT(Int_t time) {fFinalT = time;}
  inline void   SetIdil(Int_t idil)   {fIdil = idil;}
  inline void    SetICQ(Int_t  icq)     {fICQ = icq;}
  inline void  SetIHARD(Int_t hard)  {fIHARD = hard;}

  void Print();

  ClassDef(DataHeader, 1);
};

#endif
