#ifdef __CINT__

#pragma link C++ class Event+;
#pragma link C++ class EventClust+;
#pragma link C++ class Particle+;
#pragma link C++ class Fragment+;
#pragma link C++ class Baryon+;
#pragma link C++ class DataHeader+;

#endif
