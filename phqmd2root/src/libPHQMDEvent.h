#ifndef PHQMDEVENT
#define PHQMDEVENT

#include <TClonesArray.h>
#include <TMath.h>
#include <TObject.h>
#include <TNamed.h>


// ---------------------------------------------------------------------
//   Class Particle: to store particles information from phsd.dat file
// ---------------------------------------------------------------------
class Particle : public TObject {
 private:
  Int_t        fId;  // Particle number
  Int_t       fPDG;  // Particle PDG
  Int_t    fCharge;  // Particle charge
  Int_t   fChannel;  // Particle production channel
  Float_t      fPx;  // Particle Px
  Float_t      fPy;  // Particle Py
  Float_t      fPz;  // Particle Pz
  Float_t       fE;  // Particle energy
  Bool_t  fIsInMST;  // If particle was in MST clusterisation (1 = true, 0 = false)
  Bool_t fIsInSACA;  // If particle was in SACA clusterisation (1 = true, 0 = false)

 public:
  Particle();
  virtual ~Particle();

  inline Int_t   GetId()      const {return fId;}
  inline Int_t   GetPdg()     const {return fPDG;}
  inline Int_t   GetCharge()  const {return fCharge;}
  inline Int_t   GetChannel() const {return fChannel;}
  inline Float_t Px()         const {return fPx;}
  inline Float_t Py()         const {return fPy;}
  inline Float_t Pz()         const {return fPz;}
  inline Float_t E()          const {return fE;}
  inline Bool_t  IsInMST()    const {return fIsInMST;}
  inline Bool_t  IsInSACA()   const {return fIsInSACA;}
  inline Float_t Pt()         const {return TMath::Sqrt(fPx*fPx + fPy*fPy);}
  inline Float_t P()          const {return TMath::Sqrt(fPx*fPx + fPy*fPy + fPz*fPz);}
  inline Float_t Phi()        const {return TMath::ATan2(fPy, fPx);}
  inline Float_t Rapidity()   const {return 0.5 * TMath::Log((fE+fPz)/(fE-fPz));}
  inline Float_t Eta()        const {return 0.5 * TMath::Log((P() + fPz) / (P() - fPz));}

  inline void SetId      (Int_t id)          {fId = id;}
  inline void SetPdg     (Int_t pdg)       {fPDG = pdg;}
  inline void SetCharge  (Int_t q)        {fCharge = q;}
  inline void SetChannel (Int_t ch)     {fChannel = ch;}
  inline void SetPx      (Float_t px)        {fPx = px;}
  inline void SetPy      (Float_t py)        {fPy = py;}
  inline void SetPz      (Float_t pz)        {fPz = pz;}
  inline void SetE       (Float_t  e)          {fE = e;}
  inline void SetInMST   (Bool_t val)  {fIsInMST = val;}
  inline void SetInSACA  (Bool_t val) {fIsInSACA = val;}

  ClassDef(Particle, 1);
};
// ---------------------------------------------------------------------




// ---------------------------------------------------------------------
//  Class Fragment: to store particles information from 790 & 792 files
// ---------------------------------------------------------------------
class Fragment : public TObject {

 private:
  Int_t         fId;  // Fragment number
  Int_t         fnZ;  // Fragment charge (number of protons)
  Int_t         fnN;  // Neutral baryons in the fragment (S + L too!)
  Float_t       fPx;  // Px average momentum per fragment baryon
  Float_t       fPy;  // Py average momentum per fragment baryon
  Float_t       fPz;  // Pz average momentum per fragment baryon
  Float_t        fX;  // X center of mass position of fragment
  Float_t        fY;  // Y center of mass position of fragment
  Float_t        fZ;  // Z center of mass position of fragment
  Float_t fRapidity;  // Fragment rapidity
  Float_t    fAmass;  // Average fragment mass per nucleon
  Float_t     fMass;  // Fragment mass
  Int_t         fnS;  // Number of Sigma0 in the fragment
  Int_t         fnL;  // Number of Lambda in the fragment
  Float_t    fEbind;  // Binding energy of fragment [MeV]

 public:
  Fragment();
  virtual ~Fragment();

  inline Int_t   GetId()    const {return fId;}
  inline Int_t   GetA()     const {return (fnZ + fnN) > 0 ? fnZ + fnN : fnS + fnL;} // if Z+N > 0 A = Z+N, else Z=S+L
  inline Int_t   GetZ()     const {return fnZ;}
  inline Int_t   GetN()     const {return fnN;}
  inline Float_t Px()       const {return fPx * this->GetA();}
  inline Float_t Py()       const {return fPy * this->GetA();}
  inline Float_t Pz()       const {return fPz * this->GetA();}
  inline Float_t X()        const {return fX;}
  inline Float_t Y()        const {return fY;}
  inline Float_t Z()        const {return fZ;}
  inline Float_t Rapidity() const {return fRapidity;}
  inline Float_t Amass()    const {return fAmass;}
  inline Float_t Mass()     const {return fMass;}
  inline Int_t   GetS()     const {return fnS;}
  inline Int_t   GetL()     const {return fnL;}
  inline Float_t Ebind()    const {return fEbind;}
  inline Float_t Pt()       const {return TMath::Sqrt(pow(this->Px(),2) + pow(this->Py(),2));}
  inline Float_t P()        const {return TMath::Sqrt(pow(this->Px(),2) + pow(this->Py(),2) + pow(this->Pz(),2));}
  inline Float_t E()        const {return TMath::Sqrt(pow(this->Px(),2) + pow(this->Py(),2) + pow(this->Pz(),2) + pow(fMass,2));}
  inline Float_t Phi()      const {return TMath::ATan2(this->Py(), this->Px());}
  inline Float_t Eta()      const {return 0.5 * TMath::Log((this->P() + this->Pz()) / (this->P() - this->Pz()));}

  Int_t GetPdg();


  inline void SetId       (Int_t id)    {fId = id;}
  inline void SetNz       (Int_t val)   {fnZ = val;}
  inline void SetNn       (Int_t val)   {fnN = val;}
  inline void SetPx       (Float_t val) {fPx = val;}
  inline void SetPy       (Float_t val) {fPy = val;}
  inline void SetPz       (Float_t val) {fPz = val;}
  inline void SetX        (Float_t val) {fX = val;}
  inline void SetY        (Float_t val) {fY = val;}
  inline void SetZ        (Float_t val) {fZ = val;}
  inline void SetRapidity (Float_t val) {fRapidity = val;}
  inline void SetAmass    (Float_t val) {fAmass = val;}
  inline void SetMass     (Float_t val) {fMass = val;}
  inline void SetNs       (Int_t val)   {fnS = val;}
  inline void SetNl       (Int_t val)   {fnL = val;}
  inline void SetEbind    (Float_t val) {fEbind = val;}

  ClassDef(Fragment, 1);
};
// ---------------------------------------------------------------------




// ---------------------------------------------------------------------
//  Class Baryon: to store particles information from 791 & 793 files
// ---------------------------------------------------------------------
class Baryon : public TObject {

 private:
  Int_t          fId = 0;  // Baryon number
  Int_t         fBid = 0;  // Baryon MST/SACA ID
  Float_t        fPx = 0.;  // Baryon Px
  Float_t        fPy = 0.;  // Baryon Py
  Float_t        fPz = 0.;  // Baryon Pz
  Float_t         fX = 0.;  // Baryon X
  Float_t         fY = 0.;  // Baryon Y
  Float_t         fZ = 0.;  // Baryon Z
  Float_t      fMass = 0.;  // Baryon mass
  Int_t      fFragId = 0;  // Id of fragment to which this baryon belongs to
  Int_t    fFragSize = 0;  // Size of fragment to which this baryon belongs to
  Int_t      fPhsdId = 0;  // Original position of baryon in the PHSD vector
  Int_t   fFragIdMST = 0;  // Id of fragment to which this baryon belongs to (in MST)
  Int_t fFragSizeMST = 0;  // Size of fragment to which this baryon belongs to (in MST)
  Int_t  fProdRegion = 0;  // -1 - nucleon from projectile, =+1 - from target (wo collision);  = other - made collisions
  Int_t fProdChannel = 0;  // production channel of baryon according to the PHSD list (cf. PHSD Manual)
  Float_t  fProdTime = 0.;  // production time of nucleon in fm/c (needed for reconstruction of cluster stability)

 public:
  Baryon();
  virtual ~Baryon();

  inline Int_t   GetId()          const {return fId;}
  inline Int_t   GetBid()         const {return fBid;}
  inline Float_t Px()             const {return fPx;}
  inline Float_t Py()             const {return fPy;}
  inline Float_t Pz()             const {return fPz;}
  inline Float_t X()              const {return fX;}
  inline Float_t Y()              const {return fY;}
  inline Float_t Z()              const {return fZ;}
  inline Float_t Mass()           const {return fMass;}
  inline Int_t   GetFragId()      const {return fFragId;}
  inline Int_t   GetFragSize()    const {return fFragSize;}
  inline Int_t   GetPhsdId()      const {return fPhsdId;}
  inline Int_t   GetFragIdMST()   const {return fFragIdMST;}
  inline Int_t   GetFragSizeMST() const {return fFragSizeMST;}
  inline Int_t   GetProdRegion()  const {return fProdRegion;}
  inline Int_t   GetProdChannel() const {return fProdChannel;}
  inline Float_t GetProdTime()    const {return fProdTime;}


  inline Float_t Pt()             const {return TMath::Sqrt(pow(this->Px(),2) + pow(this->Py(),2));}
  inline Float_t P()              const {return TMath::Sqrt(pow(this->Px(),2) + pow(this->Py(),2) + pow(this->Pz(),2));}
  inline Float_t E()              const {return TMath::Sqrt(pow(this->Px(),2) + pow(this->Py(),2) + pow(this->Pz(),2) + pow(fMass,2));}
  inline Float_t Phi()            const {return TMath::ATan2(fPy, fPx);}
  inline Float_t Eta()            const {return 0.5 * TMath::Log((this->P() + fPz) / (this->P() - fPz));}
  inline Float_t Rapidity()       const {return 0.5 * TMath::Log((this->E()+fPz)/(this->E()-fPz));}

  inline void SetId          (Int_t id)    {fId = id;}
  inline void SetBid         (Int_t val)   {fBid = val;}
  inline void SetPx          (Float_t val) {fPx = val;}
  inline void SetPy          (Float_t val) {fPy = val;}
  inline void SetPz          (Float_t val) {fPz = val;}
  inline void SetX           (Float_t val) {fX = val;}
  inline void SetY           (Float_t val) {fY = val;}
  inline void SetZ           (Float_t val) {fZ = val;}
  inline void SetMass        (Float_t val) {fMass = val;}
  inline void SetFragId      (Int_t val)   {fFragId = val;}
  inline void SetFragSize    (Int_t val)   {fFragSize = val;}
  inline void SetPhsdId      (Int_t val)   {fPhsdId = val;}
  inline void SetFragIdMST   (Int_t val)   {fFragIdMST = val;}
  inline void SetFragSizeMST (Int_t val)   {fFragSizeMST = val;}
  inline void SetProdRegion  (Int_t val)   {fProdRegion = val;}
  inline void SetProdChannel (Int_t val)   {fProdChannel = val;}
  inline void SetProdTime    (Float_t val) {fProdTime = val;}

  ClassDef(Baryon, 1);
};
// ---------------------------------------------------------------------




// ---------------------------------------------------------------------
//             Class Event: to store event information
// ---------------------------------------------------------------------
class Event : public TObject {
 private:
  Double_t	    fB;             // Impact parameter
  Int_t         fNpart;         // Number of particles in phsd.dat event
  Int_t         fNparticipants; // Number of participants in event
  Float_t	fPsi[4];              // Participant plane angle of the n-th harmonic: 2,3,4,5
  Float_t	fEcc[4];              // Eccentricity of the n-th harmonic: 2,3,4,5
  TClonesArray* fParticles;     // Array to store particles from phsd.dat

 public:
  Event();
  virtual ~Event();
  inline Double_t GetB()             const {return fB;}
  inline Int_t    GetNpart()         const {return fNpart;}
  inline Int_t    GetNparticipants() const {return fNparticipants;}
  Float_t GetPsi(Int_t nth);
  Float_t GetEcc(Int_t nth);

  Particle* GetParticle(Int_t index) const;

  inline void SetB	           (Double_t b)  {fB = b;}
  inline void SetNpart         (Int_t Npart) {fNpart = Npart;}
  inline void SetNparticipants (Int_t Np)    {fNparticipants = Np;}
  void SetPsi(Int_t nth, Float_t val);
  void SetEcc(Int_t nth, Float_t val);

  void AddParticle(const Particle& particle);
  void Clear();

  ClassDef(Event, 1);
};
// ---------------------------------------------------------------------




// ---------------------------------------------------------------------
//             Class Event: to store event information
// ---------------------------------------------------------------------
class EventClust : public TObject {
 private:
  Int_t         fEventNr;       // Event number
  Int_t         fTimeSt;        // Event timestep
  Int_t         fNfragMST;      // Number of particles in MST (fort.790) event
  Int_t         fNfragSACA;     // Number of particles in SACA (fort.792) event
  Int_t         fNbarMST;       // Number of particles in MST (fort.791) event
  Int_t         fNbarSACA;      // Number of particles in SACA (fort.793) event
  TClonesArray* fFragmentsMST;  // Array to store particles from fort.790 (MST)
  TClonesArray* fFragmentsSACA; // Array to store particles from fort.792 (SACA)
  TClonesArray* fBaryonsMST;    // Array to store particles from fort.791 (MST)
  TClonesArray* fBaryonsSACA;   // Array to store particles from fort.793 (SACA)

 public:
  EventClust();
  virtual ~EventClust();
  inline Int_t    GetEventNr()       const {return fEventNr;}
  inline Int_t    GetTimeSt()        const {return fTimeSt;}
  inline Int_t    GetNfragMST()      const {return fNfragMST;}
  inline Int_t    GetNfragSACA()     const {return fNfragSACA;}
  inline Int_t    GetNbarMST()       const {return fNbarMST;}
  inline Int_t    GetNbarSACA()      const {return fNbarSACA;}

  Fragment* GetFragmentMST(Int_t index) const;
  Fragment* GetFragmentSACA(Int_t index) const;
  Baryon*   GetBaryonMST(Int_t index) const;
  Baryon*   GetBaryonSACA(Int_t index) const;

  inline void SetEventNr       (Int_t val)   {fEventNr = val;}
  inline void SetTimeSt        (Int_t val)   {fTimeSt = val;}
  inline void SetNfragMST      (Int_t Nfrag) {fNfragMST = Nfrag;}
  inline void SetNfragSACA     (Int_t Nfrag) {fNfragSACA = Nfrag;}
  inline void SetNbarMST       (Int_t Nbar)  {fNbarMST = Nbar;}
  inline void SetNbarSACA      (Int_t Nbar)  {fNbarSACA = Nbar;}

  void AddFragmentMST(const Fragment& fragMST);
  void AddFragmentSACA(const Fragment& fragSACA);
  void AddBaryonMST(const Baryon& baryonMST);
  void AddBaryonSACA(const Baryon& baryonSACA);
  void Clear();

  ClassDef(EventClust, 1);
};
// ---------------------------------------------------------------------



// ---------------------------------------------------------------------
//    Class DataHeader: to store infromation from the inputPHSD file
// ---------------------------------------------------------------------
class DataHeader : public TObject {
 private:
  Int_t    fAtar;    // Target mass
  Int_t    fZtar;    // Protons in target
  Int_t   fAproj;    // Projectile mass
  Int_t   fZproj;    // Protons in projectile
  Float_t  fTkin;    // Kinetic energy (GeV)
  Float_t  fBmin;    // BMIN:   minimal impact parameter in fm ! no effect for p+A
  Float_t  fBmax;    // BMAX:   maximal impact parameter in fm ! no effect for p+A
  Float_t  fDeltaB;  // DeltaB: impact parameter step in fm (used only if IBweight_MC=0)
  Int_t    fIGLUE;   // IGLUE: =1 with partonic QGP phase (PHSD mode); =0 - HSD mode
  Int_t    fIBW;     // IBweight_MC: =0 constant step in B =DBIMP; =1 choose B by Monte-Carlo ISUBS times in [Bmin,Bmax]
  Int_t    fNUM;     // NUM:    optimized number of parallel ensambles ("events")
  Int_t    fSUB;     // ISUBS:  number of subsequent runs
  Float_t  fFinalT;  // FINALT: final time of calculation in fm/c
  Int_t    fIdil;    // Idilept: =0 no dileptons; =1 electron pair; =2  muon pair
  Int_t    fICQ;     // ICQ: =0 free rho's, =1 dropping mass, =2 broadening, =3 drop.+broad.
  Int_t    fIHARD;   // IHARD: =1 with charm and bottom; =0 - without
  Int_t    fINUCLEI; //  INUCLEI  =1 reactions with deuterons (starting from 4d33166a)
  Int_t    fIPHQMD;  // IPHQMD=1: propagation with QMD dynamics; =0 with HSD/PHSD dynamics
  Int_t    fISACA;   // ISACA: enable or disable SACA output (note: SACA or MST is controled by iflagsaca)
  Float_t  fTSACA;   // TSACA: starting time for SACA
  Float_t  fDTSACA;  // DTSACA, time step for SACA calculations
  Int_t    fNTSACA;  // NTSACA, Number of SACA timesteps ( =>  tmax=tsaca+dtsaca*ntsaca must be < FINALT !)
  Int_t    fFLGSACA; // iflagsaca: =1 SACA(=FRIGA) analysis(+MST), =0 MST, no SACA --!!!! FRIGA
  Int_t    fYuk;     // tageyuk=1  ! if Yukawa potential requested in SACA       
  Int_t    fAsy;     // tageasy=1  ! if asymmetry energy requested in SACA 
  Int_t    fPair;    // tagepair=1 ! tructure effects (pairing,...) requested in SACA 
  Int_t    fCoul;    // tagecoul=0 ! activates the coulomb energy for fragment selection
  Float_t  fAsy0;    // vasy0=23.3 ! asymmetry potential energy at normal density (MeV) in SACA 
  Float_t  fEPair;   // eta_pairing=0.0 ! pairing potential exponant (0.->only forbids unbound isotopes, 1., 0.65, 0.35 or 0.25) in SACA 
  Int_t    fEOS;     // iqmdeos  ! EoS for QMD option IPHQMD=1 ; =0: hard EOS without M.D.I; =1 soft EoS
  Int_t    fResSACA; // IFLAG_Res_SACA ! =1 include ALL resonances with their decay to SACA; =2 - only nucleons; =3 nucleons and hyperons
  Int_t    fWigDens; // IfragWigDen  ! =0: no; =1 yes, light clusters formation according to the Wigner density

 public:
  DataHeader();
  virtual ~DataHeader();
  inline Int_t     GetAtar() const {return  fAtar;}
  inline Int_t     GetZtar() const {return  fZtar;}
  inline Int_t    GetAproj() const {return fAproj;}
  inline Int_t    GetZproj() const {return fZproj;}
  inline Float_t   GetTkin() const {return  fTkin;}
  inline Float_t   GetPlab() const {return TMath::Sqrt(fTkin*fTkin + 2.*0.938*fTkin);}
  inline Float_t    GetSRT() const {return TMath::Sqrt(4.*(0.938*0.938) + 2.*0.938*fTkin);}
  inline Float_t   GetBmin() const {return  fBmin;}
  inline Float_t   GetBmax() const {return  fBmax;}
  inline Float_t GetDeltaB() const {return  fDeltaB;}
  inline Int_t    GetIGlue() const {return  fIGLUE;}
  inline Int_t      GetIBW() const {return  fIBW;}
  inline Int_t      GetNum() const {return  fNUM;}
  inline Int_t      GetSub() const {return  fSUB;}
  inline Float_t GetFinalT() const {return  fFinalT;}
  inline Int_t     GetIdil() const {return  fIdil;}
  inline Int_t      GetICQ() const {return  fICQ;}
  inline Int_t    GetIHARD() const {return  fIHARD;}
  inline Int_t  GetINUCLEI() const {return  fINUCLEI;}
  inline Int_t   GetIPHQMD() const {return  fIPHQMD;}
  inline Int_t    GetISACA() const {return  fISACA;}
  Float_t  GetTSACA() const {return  fTSACA;}
  inline Float_t GetDTSACA() const {return  fDTSACA;}
  inline Int_t   GetNTSACA() const {return  fNTSACA;}
  inline Int_t  GetFLGSACA() const {return  fFLGSACA;}
  inline Int_t      GetYuk() const {return  fYuk;}
  inline Int_t      GetAsy() const {return  fAsy;}
  inline Int_t     GetPair() const {return  fPair;}
  inline Int_t     GetCoul() const {return  fCoul;}
  inline Float_t   GetAsy0() const {return  fAsy0;}
  inline Float_t  GetEPair() const {return  fEPair;}
  inline Int_t      GetEOS() const {return  fEOS;}
  inline Int_t  GetResSACA() const {return  fResSACA;}
  inline Int_t  GetWigDens() const {return  fWigDens;}

  inline void   SetAtar(Int_t  Atar)     {fAtar = Atar;}
  inline void   SetZtar(Int_t  Ztar)     {fZtar = Ztar;}
  inline void  SetAproj(Int_t Aproj)   {fAproj = Aproj;}
  inline void  SetZproj(Int_t Zproj)   {fZproj = Zproj;}
  inline void  SetTkin(Float_t Tkin)     {fTkin = Tkin;}
  inline void  SetBmin(Float_t Bmin)     {fBmin = Bmin;}
  inline void  SetBmax(Float_t Bmax)     {fBmax = Bmax;}
  inline void  SetDeltaB(Float_t dB)     {fDeltaB = dB;}
  inline void   SetIGLUE(Int_t  igl)     {fIGLUE = igl;}
  inline void     SetIBW(Int_t  ibw)       {fIBW = ibw;}
  inline void     SetNum(Int_t  num)       {fNUM = num;}
  inline void     SetSub(Int_t  sub)       {fSUB = sub;}
  inline void  SetFinalT(Int_t time)   {fFinalT = time;}
  inline void    SetIdil(Int_t flag)     {fIdil = flag;}
  inline void     SetICQ(Int_t flag)      {fICQ = flag;}
  inline void   SetIHARD(Int_t flag)    {fIHARD = flag;}
  inline void SetINUCLEI(Int_t flag)  {fINUCLEI = flag;}
  inline void  SetIPHQMD(Int_t flag)   {fIPHQMD = flag;}
  inline void   SetISACA(Int_t flag)    {fISACA = flag;}
  inline void  SetTSACA(Float_t val)     {fTSACA = val;}
  inline void SetDTSACA(Float_t val)    {fDTSACA = val;}
  inline void  SetNTSACA(Int_t flag)   {fNTSACA = flag;}
  inline void SetFLGSACA(Int_t flag)  {fFLGSACA = flag;}
  inline void     SetYuk(Int_t flag)      {fYuk = flag;}
  inline void     SetAsy(Int_t flag)      {fAsy = flag;}
  inline void    SetPair(Int_t flag)     {fPair = flag;}
  inline void    SetCoul(Int_t flag)     {fCoul = flag;}
  inline void   SetAsy0(Float_t val)      {fAsy0 = val;}
  inline void  SetEPair(Float_t val)     {fEPair = val;}
  inline void     SetEOS(Int_t flag)      {fEOS = flag;}
  inline void SetResSACA(Int_t flag)  {fResSACA = flag;}
  inline void SetWigDens(Int_t flag)  {fWigDens = flag;}

  void Print();

  ClassDef(DataHeader, 1);
};
// ---------------------------------------------------------------------

#endif
