#include <TString.h>
#include <TChain.h>
#include <TSystem.h>
#include <TFile.h>

#include "libPHQMDEvent.h"


int main(int argc, char* argv[]){
  TString outFile;
  TString inDir;
  if(argc < 3){
    printf("usage: %s <target.root> <INPUT_DIR>\n", argv[0]);
    return 1;
  }
  else{
    outFile = argv[1];
    inDir  = argv[2];
  }


  const char* ext = ".root";
  void* dirp = gSystem->OpenDirectory(inDir);

  const char* entry;
  const char* filename[100];
  Int_t n = 0;
  TString str;

// get *.root files in directory
  while((entry = (char*)gSystem->GetDirEntry(dirp))) {
    str = entry;
    if(str.EndsWith(ext))
      filename[n++] = gSystem->ConcatFileName(inDir, entry);
  }

  TFile *INfile = new TFile(filename[0], "READ");
  DataHeader *header;
  INfile -> GetObject("DataHeader", header);
  INfile -> Close();

// add them to TChain
  TFile* OUTfile = new TFile(outFile.Data(), "RECREATE");
  TChain chain("PHQMDtree");
  OUTfile->cd();
  for (Int_t i = 0; i < n; i++){
    chain.Add(filename[i]);
  }
  chain.Merge(OUTfile, 2000, "");
  printf("PHQMDtree: done\n");

  OUTfile = new TFile(outFile, "UPDATE");
  TChain chainFrag("FRAGtree");
  OUTfile->cd();
  for (Int_t i = 0; i < n; i++){
    chainFrag.Add(filename[i]);
  }
  chainFrag.Merge(OUTfile, 2000, "");
  printf("FRAGtree: done\n");

// update header
  OUTfile = new TFile(outFile, "UPDATE");
  OUTfile -> SetCompressionLevel(6);
  OUTfile -> cd();
  header -> Write();
  OUTfile -> Write();
  OUTfile -> Close();
  printf("Header: done\n");
}

