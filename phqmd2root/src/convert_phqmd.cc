#include "libPHQMDEvent.h"
#include "utils.h"

int main(int argc, char* argv[]){
  if(argc < 8){
    printf("usage: %s <inputPHSD> <phsd.dat.gz> <fort.790> <fort.791> <fort.792> <fort.793> <out.root>\n", argv[0]);
    return 1;
  }
  else{
    inputPHSD = argv[1];
    filename  = argv[2];
    mstname   = argv[3];
    f791name  = argv[4];
    sacaname  = argv[5];
    f793name  = argv[6];
  }

  TFile *OUTfile = new TFile(argv[7], "RECREATE");
  OUTfile -> SetCompressionLevel(6);



// ---------------------------------------------------------------------
// ---------------------- Header filling -------------------------------
// ---------------------------------------------------------------------
  DataHeader *header = new DataHeader();
// --- inputPHSD file reading
  Int_t Atar, Ztar, Aproj, Zproj;
  Float_t Tkin, Bmin, Bmax, DeltaB;
  Int_t NUM, SUB;
  Int_t IGLUE;
  Float_t FinalT;
  Int_t Idil, ICQ, IHARD, IBwMC, INUCLEI;
  Int_t IPHQMD, ISACA, NTSACA, FLGSACA;
  Int_t Yuk, Asy, Pair, Coul, EOS, ResSACA, WigDens;
  Float_t TSACA, DTSACA, Asy0, EPair;

  fgzFile = gzopen(inputPHSD, "rb");
  if(!fgzFile){
    printf("-E- Can not open file: %s\n", inputPHSD);
    exit(1);
  }

  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Atar);    header -> SetAtar(Atar);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Ztar);    header -> SetZtar(Ztar);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Aproj);   header -> SetAproj(Aproj);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Zproj);   header -> SetZproj(Zproj);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Tkin);    header -> SetTkin(Tkin);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Bmin);    header -> SetBmin(Bmin);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Bmax);    header -> SetBmax(Bmax);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &DeltaB);  header -> SetDeltaB(DeltaB);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &NUM);     header -> SetNum(NUM);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &SUB);     header -> SetSub(SUB);
  gzgets(fgzFile, fbuffer, 256); // ISEED
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IGLUE);   header -> SetIGLUE(IGLUE);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &FinalT);  header -> SetFinalT(FinalT);
  gzgets(fgzFile, fbuffer, 256); // ILOW
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Idil);    header -> SetIdil(Idil);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &ICQ);     header -> SetICQ(ICQ);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IHARD);   header -> SetIHARD(IHARD);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IBwMC);   header -> SetIBW(IBwMC);
  gzgets(fgzFile, fbuffer, 256); // IUSER
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &INUCLEI);  header -> SetINUCLEI(INUCLEI);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &IPHQMD);  header -> SetIPHQMD(IPHQMD);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &ISACA);   header -> SetISACA(ISACA);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &TSACA);   header -> SetTSACA(TSACA);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &DTSACA);  header -> SetDTSACA(DTSACA);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &NTSACA);  header -> SetNTSACA(NTSACA);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &FLGSACA); header -> SetFLGSACA(FLGSACA);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Yuk);     header -> SetYuk(Yuk);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Asy);     header -> SetAsy(Asy);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Pair);    header -> SetPair(Pair);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &Coul);    header -> SetCoul(Coul);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &Asy0);    header -> SetAsy0(Asy0);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%f", &EPair);   header -> SetEPair(EPair);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &EOS);     header -> SetEOS(EOS);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &ResSACA); header -> SetResSACA(ResSACA);
  gzgets(fgzFile, fbuffer, 256); sscanf(fbuffer, "%d", &WigDens); header -> SetWigDens(WigDens);
  gzclose(fgzFile);

  header -> Write();
  header -> Print();
// ---------------------------------------------------------------------
// ------------------- End of header filling ---------------------------
// ---------------------------------------------------------------------


// ------------------------ phsd.dat stuff -----------------------------
  int    phsdParts, pCharge, pChannel;
  double pB, pPx, pPy, pPz, pE;
  long   PDG;
  int    Npart = 0;
  double psi2, psi3, psi4, psi5;
  double ecc2, ecc3, ecc4, ecc5;

// -------------------- fort.790, fort.792 stuff -----------------------
  int    mstParts, sacaParts;
  int    nZ, nN, nS, nL;
  double pX, pY, pZ, Rapidity, Amass, Mass, Ebind;

// -------------------- fort.791, fort.793 stuff -----------------------
  int f791Parts, f793Parts, PHSD_ID, Bid;
  int FragId, FragSize, FragIdMST, FragSizeMST;
  int ProdRegion, ProdChannel;
  double ProdTime;
  std::vector<Int_t> IDinMST, IDinSACA;
// ---------------------------------------------------------------------


  fgzFile  = gzopen(filename, "rb");
  f791File = gzopen(f791name, "rb");
  f793File = gzopen(f793name, "rb");
  mstFile  = gzopen(mstname, "rb");
  sacaFile = gzopen(sacaname, "rb");

  if(!fgzFile){
    printf("-E- Can not open file: %s\n", filename);
    exit(1);
  }
  if(!f791File){
    printf("-E- Can not open file: %s\n", f791name);
    exit(1);
  }
  if(!f793File){
    printf("-E- Can not open file: %s\n", f793name);
    exit(1);
  }
  if(!mstFile){
    printf("-E- Can not open file: %s\n", mstname);
    exit(1);
  }
  if(!sacaFile){
    printf("-E- Can not open file: %s\n", sacaname);
    exit(1);
  }



// ---------------------------------------------------------------------
// ------------------------- Events filling ----------------------------
// ---------------------------------------------------------------------
  TTree *PHQMDtree = new TTree("PHQMDtree", "PHQMD tree");
  Event *event = new Event();
  PHQMDtree -> Branch("event", "Event", &event);

  for(Int_t isub = 0; isub < SUB; ++isub){
    for(Int_t tstep = 0; tstep <= NTSACA; ++ tstep){
      for(Int_t inum = 0; inum < NUM; ++inum){
// --------------------- Baryons from fort.791 -------------------------
        gzgets(f791File, fbuffer, 256);       // MST (fort.791): 1st line
        gzgets(f791File, fbuffer, 256);       // MST (fort.791): 2nd line
        gzgets(f791File, fbuffer, 256);       // MST (fort.791): 3rd line
        sscanf(fbuffer, "%d", &f791Parts);
        for (int i = 0; i < f791Parts; ++i){
          gzgets(f791File, fbuffer, 256);
          sscanf(fbuffer, "%*d %*d %*f %*f %*f %*f %*f %*f %*f %*d %*d %d", &PHSD_ID);
          if(tstep == NTSACA) IDinMST.push_back(PHSD_ID);
        }// MST (fort.791): end of the particles loop
// ---------------------------------------------------------------------

// --------------------- Baryons from fort.793 -------------------------
        gzgets(f793File, fbuffer, 256);       // SACA (fort.793): 1st line
        gzgets(f793File, fbuffer, 256);       // SACA (fort.793): 2nd line
        gzgets(f793File, fbuffer, 256);       // SACA (fort.793): 3rd line
        sscanf(fbuffer, "%d", &f793Parts);
        for (int i = 0; i < f793Parts; ++i){
          gzgets(f793File, fbuffer, 256);
          sscanf(fbuffer, "%*d %*d %*f %*f %*f %*f %*f %*f %*f %*d %*d %d", &PHSD_ID);
          if(tstep == NTSACA) IDinSACA.push_back(PHSD_ID);
        }// SACA (fort.793): end of the particles loop
// ---------------------------------------------------------------------

// ------------------- Particles from phsd.dat -------------------------
        if(tstep == NTSACA){
          gzgets(fgzFile, fbuffer, 256);       // phsd.dat: 1st line
          sscanf(fbuffer, "%d %*d %*d %lf", &phsdParts, &pB);
          gzgets(fgzFile, fbuffer, 256);       // phsd.dat: 2nd line
          sscanf(fbuffer, "%d %lf %lf %lf %lf %lf %lf %lf %lf", &Npart, &psi2, &ecc2, &psi3, &ecc3, &psi4, &ecc4, &psi5, &ecc5);
          for (int i = 0; i < phsdParts; ++i){
            gzgets(fgzFile, fbuffer, 256);
            sscanf(fbuffer, "%ld %d %lf %lf %lf %lf %d %*d %d", &PDG, &pCharge, &pPx, &pPy, &pPz, &pE, &pChannel, &PHSD_ID);

            Particle *particle = new Particle();
            particle -> SetId(PHSD_ID);
            particle -> SetPdg(PDG);
            particle -> SetCharge(pCharge);
            particle -> SetPx(pPx);
            particle -> SetPy(pPy);
            particle -> SetPz(pPz);
            particle -> SetE(pE);
            particle -> SetChannel(pChannel);
            if(PDG < 1000){
              particle -> SetInMST(0);
              particle -> SetInSACA(0);
            }
            else{
              auto itMST = std::find (IDinMST.begin(), IDinMST.end(), PHSD_ID);
              if (itMST != IDinMST.end()){
                particle -> SetInMST(1);
              }
              else
                particle -> SetInMST(0);

              auto itSACA = std::find (IDinSACA.begin(), IDinSACA.end(), PHSD_ID);
              if (itSACA != IDinSACA.end())
                particle -> SetInSACA(1);
              else
                particle -> SetInSACA(0);
            }
            event -> AddParticle(*particle);
          }// phsd.dat: end of the particles loop
          IDinMST.clear();
          IDinSACA.clear();

          event -> SetNpart(phsdParts);
          event -> SetB(pB);
          event -> SetNparticipants(Npart);
          event -> SetPsi(2, psi2);
          event -> SetPsi(3, psi3);
          event -> SetPsi(4, psi4);
          event -> SetPsi(5, psi5);
          event -> SetEcc(2, ecc2);
          event -> SetEcc(3, ecc3);
          event -> SetEcc(4, ecc4);
          event -> SetEcc(5, ecc5);
          PHQMDtree -> Fill();
          event -> Clear();
        }
// ---------------------------------------------------------------------
      } // NUM
    }// TimeStep
  } // SUB
  gzclose(fgzFile);
  gzclose(f791File);
  gzclose(f793File);


// ---------------------------------------------------------------------
// -------------------- Events filling - fragments ---------------------
// ---------------------------------------------------------------------
  f791File = gzopen(f791name, "rb");
  f793File = gzopen(f793name, "rb");

  TTree *FRAGtree = new TTree("FRAGtree", "Fragments tree");
  EventClust *eventFrag = new EventClust();
  FRAGtree -> Branch("eventFrag", "EventClust", &eventFrag);

  for(Int_t isub = 0; isub < SUB; ++isub){
    for(Int_t tstep = 0; tstep <= NTSACA; ++ tstep){
      for(Int_t inum = 0; inum < NUM; ++inum){
// --------------------- Baryons from fort.791 -------------------------
        gzgets(f791File, fbuffer, 256);       // MST (fort.791): 1st line
        gzgets(f791File, fbuffer, 256);       // MST (fort.791): 2nd line
        gzgets(f791File, fbuffer, 256);       // MST (fort.791): 3rd line
        sscanf(fbuffer, "%d", &f791Parts);
        for (int i = 0; i < f791Parts; ++i){
          gzgets(f791File, fbuffer, 256);
          sscanf(fbuffer, "%*d %d %lf %lf %lf %lf %lf %lf %lf %d %d %d %*d %*d %d %d %lf",
                               &Bid, &pPx, &pPy, &pPz, &pX, &pY,  &pZ,
                               &Mass, &FragId, &FragSize, &PHSD_ID,
                               &ProdRegion, &ProdChannel, &ProdTime);
          Baryon *baryon = new Baryon();
          baryon -> SetId(i);
          baryon -> SetBid(Bid);
          baryon -> SetPx(pPx);
          baryon -> SetPy(pPy);
          baryon -> SetPz(pPz);
          baryon -> SetX(pX);
          baryon -> SetY(pY);
          baryon -> SetZ(pZ);
          baryon -> SetMass(Mass);
          baryon -> SetFragId(FragId);
          baryon -> SetFragSize(FragSize);
          baryon -> SetPhsdId(PHSD_ID);
          baryon -> SetFragIdMST(FragId);
          baryon -> SetFragSizeMST(FragSize);
          baryon -> SetProdRegion(ProdRegion);
          baryon -> SetProdChannel(ProdChannel);
          baryon -> SetProdTime(ProdTime);
          eventFrag  -> AddBaryonMST(*baryon);
        }// MST (fort.791): end of the particles loop
        eventFrag -> SetNbarMST(f791Parts);
// ---------------------------------------------------------------------

// --------------------- Baryons from fort.793 -------------------------
        gzgets(f793File, fbuffer, 256);       // SACA (fort.793): 1st line
        gzgets(f793File, fbuffer, 256);       // SACA (fort.793): 2nd line
        gzgets(f793File, fbuffer, 256);       // SACA (fort.793): 3rd line
        sscanf(fbuffer, "%d", &f793Parts);
        for (int i = 0; i < f793Parts; ++i){
          gzgets(f793File, fbuffer, 256);
          sscanf(fbuffer, "%*d %d %lf %lf %lf %lf %lf %lf %lf %d %d %d %d %d %d %d %lf",
                               &Bid, &pPx, &pPy, &pPz, &pX, &pY,  &pZ,
                               &Mass, &FragId, &FragSize, &PHSD_ID, &FragIdMST, &FragSizeMST,
                               &ProdRegion, &ProdChannel, &ProdTime);
          Baryon *baryon = new Baryon();
          baryon -> SetId(i);
          baryon -> SetBid(Bid);
          baryon -> SetPx(pPx);
          baryon -> SetPy(pPy);
          baryon -> SetPz(pPz);
          baryon -> SetX(pX);
          baryon -> SetY(pY);
          baryon -> SetZ(pZ);
          baryon -> SetMass(Mass);
          baryon -> SetFragId(FragId);
          baryon -> SetFragSize(FragSize);
          baryon -> SetPhsdId(PHSD_ID);
          baryon -> SetFragIdMST(FragIdMST);
          baryon -> SetFragSizeMST(FragSizeMST);
          baryon -> SetProdRegion(ProdRegion);
          baryon -> SetProdChannel(ProdChannel);
          baryon -> SetProdTime(ProdTime);
          eventFrag -> AddBaryonSACA(*baryon);
        }// SACA (fort.793): end of the particles loop
        eventFrag -> SetNbarSACA(f793Parts);
// ---------------------------------------------------------------------

// ------------------- Fragments from fort.790 -------------------------
        gzgets(mstFile, fbuffer, 256);       // MST (fort.790): 1st line
        gzgets(mstFile, fbuffer, 256);       // MST (fort.790): 2nd line
        gzgets(mstFile, fbuffer, 256);       // MST (fort.790): 3rd line
        sscanf(fbuffer, "%d", &mstParts);
        for (int i = 0; i < mstParts; ++i){
          gzgets(mstFile, fbuffer, 256);
          sscanf(fbuffer, "%*d %d %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %d %d %lf",
                               &nZ, &nN, 
                               &pPx, &pPy, &pPz,
                               &pX,   &pY,  &pZ, 
                               &Rapidity, &Amass, &Mass,
                               &nL, &nS, &Ebind);

          Fragment *fragment = new Fragment();
          fragment -> SetId(i);
          fragment -> SetNz(nZ);
          fragment -> SetNn(nN);
          fragment -> SetPx(pPx);
          fragment -> SetPy(pPy);
          fragment -> SetPz(pPz);
          fragment -> SetX(pX);
          fragment -> SetY(pY);
          fragment -> SetZ(pZ);
          fragment -> SetRapidity(Rapidity);
          fragment -> SetAmass(Amass);
          fragment -> SetMass(Mass);
          fragment -> SetNs(nS);
          fragment -> SetNl(nL);
          fragment -> SetEbind(Ebind);
          
          eventFrag -> AddFragmentMST(*fragment);
        }// MST (fort.790): end of the particles loop
        eventFrag -> SetNfragMST(mstParts);
// ---------------------------------------------------------------------

// ------------------- Fragments from fort.792 -------------------------
        gzgets(sacaFile, fbuffer, 256);       // SACA (fort.792): 1st line
        gzgets(sacaFile, fbuffer, 256);       // SACA (fort.792): 2nd line
        gzgets(sacaFile, fbuffer, 256);       // SACA (fort.792): 3rd line
        sscanf(fbuffer, "%d", &sacaParts);
        for (int i = 0; i < sacaParts; ++i){
          gzgets(sacaFile, fbuffer, 512);
          sscanf(fbuffer, "%*d %d %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %d %d %lf", 
                               &nZ, &nN, 
                               &pPx, &pPy, &pPz,
                               &pX,   &pY,  &pZ, 
                               &Rapidity, &Amass, &Mass,
                               &nL, &nS, &Ebind);

          Fragment *fragment = new Fragment();
          fragment -> SetId(i);
          fragment -> SetNz(nZ);
          fragment -> SetNn(nN);
          fragment -> SetPx(pPx);
          fragment -> SetPy(pPy);
          fragment -> SetPz(pPz);
          fragment -> SetX(pX);
          fragment -> SetY(pY);
          fragment -> SetZ(pZ);
          fragment -> SetRapidity(Rapidity);
          fragment -> SetAmass(Amass);
          fragment -> SetMass(Mass);
          fragment -> SetNs(nS);
          fragment -> SetNl(nL);
          fragment -> SetEbind(Ebind);
          
          eventFrag -> AddFragmentSACA(*fragment);
        }// SACA (fort.792): end of the particles loop
        eventFrag -> SetNfragSACA(sacaParts);
// ---------------------------------------------------------------------

        eventFrag -> SetEventNr(isub*NUM + inum);
        eventFrag -> SetTimeSt(tstep);
        FRAGtree -> Fill();
        eventFrag -> Clear();
      } // NUM
    } // TIME
  }// SUB




  gzclose(f791File);
  gzclose(f793File);
  gzclose(mstFile);
  gzclose(sacaFile);

  OUTfile -> Write();
  OUTfile -> Close();

  return 0;
}
