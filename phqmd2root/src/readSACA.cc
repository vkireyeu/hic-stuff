/* 
 * Sample program to read PHQMD ROOT-files, if there are more than one 
 * time step, it takes only the last one.
 * 
 * Program construct full events using fragments and baryons 
 * from SACA (fort.792) and mesons as well as baryons not in 
 * clusterization (IsInSACA = 0) from phsd.dat.
 * If "not allowed" fragment constructed is found, 
 * SACA baryons array (fort.793) is readed 
 * to find particles belonging to that fragment. Then the PHSD_ID of 
 * corresponding particles from 'phsd.dat' is checked and these 
 * original particles replace "not allowed" fragment in the resulting
 * event.
 * Allowed fragments are defined in the "AllowedFragments" vector.
 * 
 * Verbosity level is controlled by the INFO[*] flag check.
 * INFO[0] - print if particle in phsd.dat is not in SACA
 * INFO[1] - print SACA particle PDG code
 * INFO[2] - print "not allowed" fragment info
 * INFO[3] - print event "header"
 * INFO[4] - print protons and neutrons check
 * INFO[5] - some general print
 * Possible values: 1 - enable print, 0 - disable print
 * 
 * Usage:
 * ./read input.root output.dat hists.root
 * 
 * Output file has 'phsd.dat' format.
 * 
 * V. Kireyeu, vkireyeu@jinr.ru, vkireyeu@cern.ch
 */

#include "libPHQMDEvent.h"
#include "utils.h"

#include <algorithm>
#include <vector>
#include <TH1D.h>

static const bool INFO[6] = {0, 0, 0, 0, 0, 1};

static const std::vector<Int_t> AllowedFragments = {2212, 2112, 3122, 3212,
                                                    1000010020, 1000010030,
                                                    1000020030, 1000020040,
                                                    1000030060, 1000030070,
                                                    1000040090,
                                                    1010010030, 1010020040,
                                                    1010010040, 1020010040,
                                                    1010020050, 1020020050,
                                                    1020010050, 1020020060};

int main(int argc, char* argv[]){
  if(argc < 3){
    printf("usage: %s <in.root> <out.dat>\n", argv[0]);
    return 1;
  }

  TFile *INfile = new TFile(argv[1], "READ");               // Open ROOT file
  DataHeader *header;                                       // Header = inputPHSD file
  INfile -> GetObject("DataHeader", header);                // Get it from file
  if(INFO[5]) header -> Print();                            // Print header
  Int_t NTimeSteps = header->GetNTSACA() + 1;               // Total time steps = NTSACA + 1
  Int_t NUMS = header->GetNum();                            // Extract NUM from header

// --------------------------- phsd.dat --------------------------------
  TTree    *PHQMDtree = (TTree*) INfile->Get("PHQMDtree");  // Tree with 'phsd.dat' information
  Event    *fEvent = new Event();                           // Event with 'phsd.dat' particles
  PHQMDtree -> SetBranchAddress("event", &fEvent);          // Tell fEvent to take info from 'event' branch in the root-file
  int NumEvtsPHSD = PHQMDtree -> GetEntries();              // Number of 'phsd.dat' events
  if(INFO[5]) printf("Input file has %d events in the PHQMD tree\n", NumEvtsPHSD);
  Particle *fParticle = new Particle();                     // Particle from phsd.dat
// ---------------------------------------------------------------------

// ----------------------------- fort.* --------------------------------
  TTree      *FRAGtree = (TTree*) INfile->Get("FRAGtree");  // Tree with fragments
  EventClust *fEventFrag = new EventClust();                // Event with fragments
  FRAGtree -> SetBranchAddress("eventFrag", &fEventFrag);   // Tell fEventFrag to take info from 'eventFrag' branch in the root-file
  int NumEvtsFrag = FRAGtree -> GetEntries();               // Total number of events = NUM*SUB*NTSACA
  if(INFO[5]) printf("Input file has %d events in the Fragments tree with %d timestep(s)\n", NumEvtsFrag, NTimeSteps);
  Baryon     *fBaryonSACA = new Baryon();                    // SACA baryon from fort.792
  Fragment   *fFragmentSACA = new Fragment();                // SACA fragment from fort.792
// ---------------------------------------------------------------------

// ----------------------------- ASCII ---------------------------------
  FILE *phsddat;                                            // ASCII file for 'phsd.dat'-like output
  if ((phsddat = fopen(argv[2], "w"))==NULL) {
    printf("Cannot open output file %s.\n", argv[2]);
    exit(1);
  }
// ---------------------------------------------------------------------


// ---------------------------------------------------------------------
  for(int iterEvt = 0; iterEvt < NumEvtsFrag; ++iterEvt){
    FRAGtree  -> GetEntry(iterEvt);                                  // Open an event in "frags" tree
    if(fEventFrag -> GetTimeSt() != header -> GetNTSACA()) continue; // Select only last timestep
    if(INFO[3]) printf("Iter: %d, frag. event nr: %d\n", iterEvt, fEventFrag -> GetEventNr());
    PHQMDtree -> GetEntry(fEventFrag -> GetEventNr());               // Open corresponding event in the "phsd.dat" tree
    int NPartPHSD = fEvent     -> GetNpart();                        // Get number of "phsd.dat" particles
    int NFragSACA  = fEventFrag -> GetNfragSACA();                     // Get number of "fort.792" particles
    int NBarSACA   = fEventFrag -> GetNbarSACA();                      // Get number of "fort.793" particles
    if(INFO[3]) printf("Event %3d, particles in phsd.dat: %d, particles in fort.792: %d\n", iterEvt, NPartPHSD, NFragSACA);


// ----------- Calculate number of the full event particles ------------// All this block is needed just for the one number
    Int_t NOutPart = 0;                                                 // in the output ASCII file - number of full event particles
    for (int i=0; i < NPartPHSD; ++i){
      fParticle = fEvent->GetParticle(i);
      if(!fParticle -> IsInSACA()){
        ++NOutPart;                           // Take 'not in SACA' particles from 'phsd.dat'
      }
    }
    for (int i=0; i < NFragSACA; ++i){
      fFragmentSACA = fEventFrag->GetFragmentSACA(i);
      Int_t A = fFragmentSACA -> GetA();
      if(std::find(AllowedFragments.begin(),
                   AllowedFragments.end(),
                   fFragmentSACA -> GetPdg()) != AllowedFragments.end()){
        ++NOutPart;                                                     // Take SACA fragment/baryon
      }
      else{
        NOutPart += A;                                            // or divide 'not allowed' fragment to single particles and take them
      }
    }// SACA particles
// ---------------------------------------------------------------------


// ---------------------------------------------------------------------
// ---------------------- Full event construction ----------------------// make 'phsd.dat' great again!
// ---------------------------------------------------------------------
    Int_t SUB = iterEvt / NUMS;
    Int_t NUM = iterEvt - NUMS*SUB;
    fprintf(phsddat, "%6d %10d %6d  %10E    1\n", NOutPart, SUB+1, NUM+1, fEvent->GetB());  // Event header filling - 2 lines
    fprintf(phsddat, "%6d %E %E %E %E %E %E %E %E\n", fEvent -> GetNparticipants(),
                                                      fEvent -> GetPsi(2), fEvent -> GetEcc(2),
                                                      fEvent -> GetPsi(3), fEvent -> GetEcc(3),
                                                      fEvent -> GetPsi(4), fEvent -> GetEcc(4),
                                                      fEvent -> GetPsi(5), fEvent -> GetEcc(5));
    Int_t nZ = 0, nN = 0;
    for (int i=0; i < NFragSACA; ++i){
      fFragmentSACA = fEventFrag->GetFragmentSACA(i);
      Int_t Z = fFragmentSACA -> GetZ();
      Int_t N = fFragmentSACA -> GetN();
      Int_t S = fFragmentSACA -> GetS();
      Int_t L = fFragmentSACA -> GetL();
      Int_t A = fFragmentSACA -> GetA();
      if(N > 1) N = N - (L + S); // Calculate the right number of neutrons if it's a fragment
      nZ += Z;
      nN += N;

      if(INFO[1]) printf("Particle PGG code in fort.792: %11d\n", fFragmentSACA -> GetPdg());
      if(std::find(AllowedFragments.begin(),                   // "Allowed" fragment
                   AllowedFragments.end(),
                   fFragmentSACA -> GetPdg()) != AllowedFragments.end()){
        fprintf(phsddat, "%12d %6d %+3.8E %+3.8E %+3.8E %+3.8E %8d %8d %8d\n",
          fFragmentSACA -> GetPdg(), fFragmentSACA -> GetZ(),
          fFragmentSACA -> Px(), fFragmentSACA -> Py(), fFragmentSACA -> Pz(), 
          TMath::Sqrt(pow(fFragmentSACA -> Px(), 2) + pow(fFragmentSACA -> Py(), 2) + pow(fFragmentSACA -> Pz(), 2)  + pow(fFragmentSACA -> Mass(), 2)),
          999, 1, fFragmentSACA -> GetId()+1);
      }
      else{                                                    // "Not allowed" fragment case
        Int_t FoundFrags = 0;
        if(INFO[2]) printf("Impossible fragment found! Id: %d, A: %3d, Z: %3d, N: %3d, L: %3d, S: %3d (%d)\n", i+1, A,
         Z, N, L, S, fFragmentSACA -> GetPdg());
        for (int j=0; j < NBarSACA; ++j){                       // open 'baryonsSACA' fort.793 array
          fBaryonSACA = fEventFrag->GetBaryonSACA(j);
          Int_t ShiftedFragId = i+1; // (it starts from 0)
          if(fBaryonSACA -> GetFragId() == ShiftedFragId){      // and find particles which makes this fragment
            Int_t PHSD_ID = fBaryonSACA -> GetPhsdId();         // to take the ID of the corresponding particle in 'phsd.dat'
            if(INFO[2]) printf("PHSD ID: %d ", PHSD_ID);
            for (int k=0; k < NPartPHSD; ++k){
              fParticle = fEvent->GetParticle(k);
              if (fParticle -> GetId() == PHSD_ID){            // find these original particles to take their info
                ++FoundFrags;
                if(INFO[2]) printf("=== %d [%3d]\n", fParticle -> GetPdg(), FoundFrags);
                fprintf(phsddat, "%12d %6d %+3.8E %+3.8E %+3.8E %+3.8E %8d %8d %8d\n",
                  fParticle -> GetPdg(), fParticle -> GetCharge(),
                  fParticle -> Px(), fParticle -> Py(), fParticle -> Pz(), fParticle -> E(),
                  fParticle -> GetChannel(), 0, fParticle -> GetId());
                break;
              }
            } // loop over phsd.dat
            if(FoundFrags == A) break; // to save some time
          }
        } // loop over SACA Baryons
      }
    }// SACA particles



// ---------- loop over 'phsd.dat' particles
    Int_t nProt = 0, nNeut = 0;
    for (int i=0; i < NPartPHSD; ++i){
      fParticle = fEvent->GetParticle(i);
      if(!fParticle -> IsInSACA()){
        if(INFO[0]) printf("Particle not in fragmentation found: %5d\n", fParticle -> GetPdg());
        fprintf(phsddat, "%12d %6d %+3.8E %+3.8E %+3.8E %+3.8E %8d %8d %8d\n",
          fParticle -> GetPdg(), fParticle -> GetCharge(),
          fParticle -> Px(), fParticle -> Py(), fParticle -> Pz(), fParticle -> E(),
          fParticle -> GetChannel(), fParticle -> IsInSACA(), fParticle -> GetId());
      }
      if(fParticle -> GetPdg() == 2212) ++nProt;
      if(fParticle -> GetPdg() == 2112) ++nNeut;
    }
// ----------- 




    if(INFO[4]) printf("phsd.dat: %3d protons, %3d neutrons\n", nProt, nNeut);
    if(INFO[4]) printf("fort.792: %3d protons, %3d neutrons\n", nZ, nN);
  } // Full event
// ---------------------- Full event is ready! -------------------------
  fclose(phsddat);

  return 0;
}
