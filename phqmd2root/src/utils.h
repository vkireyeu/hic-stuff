#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include <TApplication.h>
#include <TSystem.h>


#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include <math.h>

char       fbuffer[256];

gzFile     fgzFile;
gzFile     mstFile;
gzFile     f791File;
gzFile     sacaFile;
gzFile     f793File;

const char *filename;
const char *mstname;
const char *f791name;
const char *sacaname;
const char *f793name;

const char *inputPHSD;


struct barfrag{
  Int_t   id;
  Int_t   nZ;
  Float_t Px;
  Float_t Py;
  Float_t Pz;
  Float_t X;
  Float_t Y;
  Float_t Z; 
  Float_t Mass; 
  Int_t   FragId;
  Int_t   FragSize;
  Int_t   FragIdMST;
  Int_t   FragSizeMST;
  Int_t   PHSD_ID;
  Int_t   tstep;
};
