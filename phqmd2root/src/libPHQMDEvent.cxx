#include <algorithm>

#include <TObject.h>
#include <TClonesArray.h>

#include "libPHQMDEvent.h"

// ---------------------------------------------------------------------
//   Class Particle: to store particles information from phsd.dat file
// ---------------------------------------------------------------------
Particle::Particle(){
  fId = fPDG = fCharge = fChannel = 0;
  fPx = fPy = fPz = fE = 0.;
}

Particle::~Particle(){  // Destructor
}
// ---------------------------------------------------------------------




// ---------------------------------------------------------------------
//  Class Fragment: to store particles information from 790 & 792 files
// ---------------------------------------------------------------------
Fragment::Fragment(){
  fId = fnZ = fnN = fnS = fnL = 0;
  fPx = fPy = fPz = fX = fY = fZ = 0.;
  fRapidity = fAmass = fMass = fEbind = 0.;
}

Int_t Fragment::GetPdg(){
  Int_t I = 0;
  Int_t PDG = 0;

  if      (fnN == 1 && fnZ ==  0 && fnL == 0 && fnS == 0) PDG = 2112; // neutron
  else if (fnN == 0 && fnZ ==  1 && fnL == 0 && fnS == 0) PDG = 2212; // proton
  else if (fnN == 0 && fnZ ==  0 && fnL == 1 && fnS == 0) PDG = 3122; // lambda0
  else if (fnN == 0 && fnZ ==  0 && fnL == 0 && fnS == 1) PDG = 3212; // sigma0

  else{
    Int_t LS = fnL + fnS;
    //fnN = fnN - LS;
    Int_t A = fnN + fnZ;
    PDG = 10*10E7 + LS*10E6 + fnZ*10E3 + A*10 + I;
  }
  return PDG;
};

Fragment::~Fragment(){  // Destructor
}
// ---------------------------------------------------------------------


// ---------------------------------------------------------------------
//  Class Baryon: to store particles information from 791 & 793 files
// ---------------------------------------------------------------------
Baryon::Baryon(){
}

Baryon::~Baryon(){  // Destructor
}
// ---------------------------------------------------------------------



// ---------------------------------------------------------------------
//             Class Event: to store event information
// ---------------------------------------------------------------------
Event::Event(){
  fB = 0.;
  fNpart = fNparticipants = 0;
  std::fill(fPsi, fPsi+4, 0);
  std::fill(fEcc, fEcc+4, 0);
  fParticles     = new TClonesArray("Particle", 100);
}

Event::~Event(){
  Clear();
  delete fParticles;
}

Float_t Event::GetPsi(Int_t nth){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
    return -1;
  }
  return fPsi[nth-2];
}

Float_t Event::GetEcc(Int_t nth){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
    return -1;
  }
  return fEcc[nth-2];
}

void Event::SetPsi(Int_t nth, Float_t val){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
  }
  else fPsi[nth-2] = val;
}

void Event::SetEcc(Int_t nth, Float_t val){
  if(nth < 2 || nth > 5){
    printf("Harmonics can be 2,3,4,5\n");
  }
  else fEcc[nth-2] = val;
}

void Event::AddParticle(const Particle& particle){
  new ((*fParticles)[fNpart]) Particle(particle);
  fNpart += 1;
}

void Event::Clear(){
  fParticles     -> Clear();
  fB = 0.;
  fNpart = fNparticipants = 0;
  std::fill(fPsi, fPsi+4, 0);
  std::fill(fEcc, fEcc+4, 0);
}

Particle* Event::GetParticle(Int_t index) const{
  if(index < 0) {
    return NULL;
  }
  if(index >= fNpart) {
    return NULL;
  }
  return ((Particle*) fParticles->At(index));
}
// ---------------------------------------------------------------------




// ---------------------------------------------------------------------
//             Class EventClust: to store EventClust information
// ---------------------------------------------------------------------
EventClust::EventClust(){
  fEventNr = fTimeSt = 0;
  fNfragMST = fNbarMST = 0;
  fNfragSACA = fNbarSACA = 0;
  fFragmentsMST  = new TClonesArray("Fragment", 100);
  fFragmentsSACA = new TClonesArray("Fragment", 100);
  fBaryonsMST    = new TClonesArray("Baryon", 100);
  fBaryonsSACA   = new TClonesArray("Baryon", 100);
}

EventClust::~EventClust(){
  Clear();
  delete fFragmentsMST;
  delete fFragmentsSACA;
  delete fBaryonsMST;
  delete fBaryonsSACA;
}

void EventClust::AddFragmentMST(const Fragment& fragMST){
  new ((*fFragmentsMST)[fNfragMST]) Fragment(fragMST);
  fNfragMST += 1;
}

void EventClust::AddFragmentSACA(const Fragment& fragSACA){
  new ((*fFragmentsSACA)[fNfragSACA]) Fragment(fragSACA);
  fNfragSACA += 1;
}

void EventClust::AddBaryonMST(const Baryon& baryonMST){
  new ((*fBaryonsMST)[fNbarMST]) Baryon(baryonMST);
  fNbarMST += 1;
}

void EventClust::AddBaryonSACA(const Baryon& baryonSACA){
  new ((*fBaryonsSACA)[fNbarSACA]) Baryon(baryonSACA);
  fNbarSACA += 1;
}

void EventClust::Clear(){
  fFragmentsMST  -> Clear();
  fFragmentsSACA -> Clear();
  fBaryonsMST    -> Clear();
  fBaryonsSACA   -> Clear();
  fNfragMST = fNbarMST = 0;
  fNfragSACA = fNbarSACA = 0;
}

Fragment* EventClust::GetFragmentMST(Int_t index) const{
  if(index < 0) {
    return NULL;
  }
  if(index >= fNfragMST) {
    return NULL;
  }
  return ((Fragment*) fFragmentsMST->At(index));
}

Fragment* EventClust::GetFragmentSACA(Int_t index) const{
  if(index < 0) {
    return NULL;
  }
  if(index >= fNfragSACA) {
    return NULL;
  }
  return ((Fragment*) fFragmentsSACA->At(index));
}

Baryon* EventClust::GetBaryonMST(Int_t index) const{
  if(index < 0) {
    return NULL;
  }
  if(index >= fNbarMST) {
    return NULL;
  }
  return ((Baryon*) fBaryonsMST->At(index));
}

Baryon* EventClust::GetBaryonSACA(Int_t index) const{
  if(index < 0) {
    return NULL;
  }
  if(index >= fNbarSACA) {
    return NULL;
  }
  return ((Baryon*) fBaryonsSACA->At(index));
}
// ---------------------------------------------------------------------



// ---------------------------------------------------------------------
//    Class DataHeader: to store infromation from the inputPHSD file
// ---------------------------------------------------------------------
DataHeader::DataHeader(){
  fAtar  = fZtar = fAproj = fZproj = 0;
  fTkin = fBmin = fBmax = fDeltaB = fFinalT = 0.;
  fIGLUE = fIBW = fNUM = fSUB = 0;
  fIdil = fICQ = fIHARD = fINUCLEI = 0;
  fIPHQMD = fISACA = 0;
  fTSACA = fDTSACA = 0.;
  fYuk = fAsy = fPair = fCoul = 0;
  fAsy0 = fEPair = 0.;
  fEOS = fResSACA = fWigDens = 0;
}

DataHeader::~DataHeader(){  // Destructor
}


void DataHeader::Print(){
  printf("Target(A,Z): %4d, %4d\n", fAtar, fZtar);
  printf("Projectile(A,Z): %4d, %4d\n", fAproj, fZproj);
  printf("Tkin: %8.2f, Plab: %8.2f, Sqrt(S): %8.2f\n", fTkin, this->GetPlab(), this->GetSRT());
  printf("Bmin: %4.2f, Bmax: %4.2f, DeltaB: %4.2f, IBweight_MC=%1d\n", fBmin, fBmax, fDeltaB, fIBW);
  printf("NUM: %4d, SUB: %4d\n", fNUM, fSUB);
  printf("FinalT: %4.2f\n", fFinalT);
  printf("IGLUE=%1d, Idilept=%1d, ICQ=%1d, IHARD=%1d\n", fIGLUE, fIdil, fICQ, fIHARD);
  printf("INUCLEI=%1d\n", fINUCLEI);
  printf("IPHQMD=%1d, ISACA=%1d\n", fIPHQMD, fISACA);
  printf("TSACA=%.2f, DTSACA=%.2f\n", fTSACA, fDTSACA);
  printf("NTSACA=%1d, IFagSACA=%1d\n", fNTSACA, fFLGSACA);
  printf("YUK=%1d, ASY=%1d, PAIR=%1d, COUL=%1d\n", fYuk, fAsy, fPair, fCoul);
  printf("ASY0=%.2f, EPair=%.2f\n", fAsy0, fEPair);
  printf("EOS=%1d, ResSACA=%1d, WigDens=%1d\n", fEOS, fResSACA, fWigDens);
}
// ---------------------------------------------------------------------

ClassImp(Event);
ClassImp(Particle);
ClassImp(Fragment);
ClassImp(Baryon);
ClassImp(DataHeader);
