# phsd2hepmc
Converter from the 'phsd.dat' to the [HepMC2](http://hepmc.web.cern.ch/hepmc/) format.  
This converter was born in October-November of 2019 with the support on the [HepMC](https://gitlab.cern.ch/hepmc/HepMC) format from Andy Buckley and Andrii Verbytskyi‎ and [Christian Bierlich](https://bierlich.net/) from the [Rivet](https://rivet.hepforge.org/) side. It was used for several plots
in the [Hadron production in elementary nucleon–nucleon reactions from low to ultra-relativistic energies](https://doi.org/10.1140/epja/s10050-020-00232-7).  
I would also acknowledge Ilia Grishmanovskii from the [PHSD](https://theory.gsi.de/~ebratkov/phsd-project/PHSD/) team for the bugs finding.


Files in this format may be used for the analysis with the [Rivet toolkit](https://rivet.hepforge.org/).  
This converter works with elementary, heavy-ion and asymmetric systems.  

Please, download and install HepMC2 library before using this convertor.
To compile converter put the full path to the HepMC2 installation to the "HEPMC2_DIR" variable in the Makefile and run 'make'.
Usage:
```
./phsd2hepmc2 <PDG1> <PDG2> <E1> <E2> <phsd.dat.gz> <output.hepmc>
```

## Authors
* **Viktar Kireyeu** - *Initial work*  


### Known issues
If you are working on the GSI cluster, you may use new GCC compilers version (gcc-4.9 is broken), e.g.:
```
$ module use /cvmfs/it.gsi.de/modulefiles/
$ module load cmake/3.15.3
$ module load compiler/gcc/9.2.0
```

