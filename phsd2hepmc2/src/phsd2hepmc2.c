#include "utils.h"
#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"

const double kProtonMass = 0.938272;
using namespace HepMC;

void calc_xs(const double SRT, double& SinelNN, double& SelNN, double& SigTot);

int main(int argc, char* argv[]){
  int beamPart1 =   0, beamPart2 =   0;
  int Amass1    =   0, Amass2    =   0;
  double beamE1 = 0.0, beamE2    = 0.0;

  if(argc < 7){
    printf("usage: %s <PDG1> <PDG2> <E1> <E2> <phsd.dat.gz> <output.hepmc>\n", argv[0]);
    return 1;
  }
  else{
    beamPart1 = atoi(argv[1]);
    beamPart2 = atoi(argv[2]);
    beamE1    = atof(argv[3]);
    beamE2    = atof(argv[4]);
    filename  = argv[5];
    outfile   = argv[6];
  }



// Beams kinematics
//S----------------------------------------------------------------
  bool beam1hi = 0;
  bool beam2hi = 0;

  if(beamPart1 > 9999){
    Amass1 = beamPart1 % 10000 / 10;
    beam1hi = 1;
  }
  else{
    Amass1 = 1;
  }

  if(beamPart2 > 9999){
    Amass2 = beamPart2 % 10000 / 10;
    beam2hi = 1;
  }
  else{
    Amass2 = 1;
  }

  double massBeam1 = Amass1 * 0.938;
  double massBeam2 = Amass2 * 0.938;

//  double beamsEtot = (SRT / 2.0);
  double beamE1tot = beamE1 * Amass1;
  double beamE2tot = beamE2 * Amass2;

  double beamPz1   = sqrt(pow(beamE1tot, 2) - pow(massBeam1, 2));
  double beamPz2   = sqrt(pow(beamE2tot, 2) - pow(massBeam2, 2));

  double SRT       = sqrt(pow((beamE1 + beamE2), 2) - pow((beamPz1/(double)Amass1 - beamPz2/(double)Amass2),2));

  printf("Beams: %d %d\n",                 Amass1,    Amass2);
  printf("Beams masses: %8.4f %8.4f\n", massBeam1, massBeam2);
  printf("Beam momenta: %8.4f %8.4f\n",   beamPz1,   beamPz2);
  printf("Beam energy: %8.4f %8.4f\n",  beamE1tot, beamE2tot);
  printf("SQRT(s_NN): %8.4f\n",                          SRT);

//E----------------------------------------------------------------


// --- ASCII files stuff
  int    pBparts;
  long   PDG;
  int    nInelastic = 0;
  double pB, pPx, pPy, pPz, pE;


// --- HepMC2 stuff
  HepMC::IO_GenEvent ascii_out(outfile, std::ios::out);
  double SinelNN = 0.0, SelNN = 0.0, SigTot = 0.0;
  calc_xs(SRT, SinelNN, SelNN, SigTot);
  printf("%lf %lf\n", SinelNN, SinelNN*1e-9);
  GenEvent*  evt = NULL;
  GenVertex*  v1 = NULL;
  GenParticle* p;
  GenParticle* beamp1;
  GenParticle* beamp2;
  GenCrossSection xs;
  PdfInfo  pdf(0, 0, 0, 0, 0, 0, 0);
  HeavyIon hic(0, 0, 0, 0, 0, 0);
  WeightContainer wgth(1, 1.0);
  xs.set_cross_section(SinelNN*1e+9, 0.0);

// ASCII - read + calculate
//S----------------------------------------------------------------
  fgzFile = gzopen(filename, "rb");
  if(!fgzFile){
    printf("-E- Can not open file: %s\n", filename);
    exit(1);
  }

  while (gzgets(fgzFile, fbuffer, 256) != NULL){
    if (gzeof(fgzFile)){
      gzclose(fgzFile);
      fgzFile=NULL;
      break;
    }

    sscanf(fbuffer, "%d %*d %*d %lf", &pBparts, &pB);
    gzgets(fgzFile, fbuffer, 256);	 // 2nd line


    if(pBparts > 2){
      ++nInelastic;
      beamp1 = new GenParticle(FourVector(0, 0,  beamPz1, beamE1tot), beamPart1, 4);
      beamp2 = new GenParticle(FourVector(0, 0, -beamPz2, beamE2tot), beamPart2, 4);

      v1  = new GenVertex();
      evt = new GenEvent(101, nInelastic-1, v1, wgth);
      if(beam1hi || beam2hi){
        hic.set_impact_parameter(pB);
        evt -> set_heavy_ion(hic);
      }
      else{
        evt -> set_pdf_info(pdf);
      }
      evt -> set_cross_section(xs);
      evt -> set_event_scale(0);
      evt -> set_alphaQCD(0);
      evt -> set_alphaQED(0);
      evt -> add_vertex(v1);
      v1  -> add_particle_in(beamp1);
      v1  -> add_particle_in(beamp2);
      evt -> set_beam_particles(beamp1, beamp2);
    }

    // --- loop over particles
    for (int i = 0; i < pBparts; ++i){
      gzgets(fgzFile, fbuffer, 256);
      if(pBparts <= 2) continue;
      sscanf(fbuffer, "%ld %*d %lf %lf %lf %lf", &PDG, &pPx, &pPy, &pPz, &pE);
      p  = new GenParticle(FourVector(pPx, pPy, pPz, pE), PDG, 1);
      v1 -> add_particle_out(p);
    }// end of the particles (baryons or phsd.dat) loop

    if(pBparts > 2){
      evt -> set_signal_process_vertex(v1);
//      evt -> print();
      ascii_out << evt;
      delete evt;
    }
  }
  gzclose(fgzFile);
//E----------------------------------------------------------------

  printf("Done: %d events\n", nInelastic);
  return 0;
}




// Cross-sections calculation from PHSD
// Cross-sections calculation from PHSD
void calc_xs(const double SRT, double& SinelNN, double& SelNN, double& SigTot){

  double MOMENT, MOMEN8;
  SinelNN = 0.0; SelNN = 0.0; SigTot = 0.0;

  if(SRT <= 2.0 * kProtonMass){
    printf("-E: Energy too low\n");
    exit(1);
  }

  MOMEN8 = pow(SRT,4) / (4.0 * pow(kProtonMass, 2)) - pow(SRT, 2);
  MOMENT = 0.1;

  if(MOMEN8 > 1.E-2) MOMENT = sqrt(MOMEN8);

  SigTot = 48.0 + 0.522 * pow(log(MOMENT), 2) - 4.51 * log(MOMENT);
  if (MOMENT < 3.0) SigTot = 43.7;

  // CALCULATION OF ELASTIC CROSS SECTION
  double XIMPULS = sqrt(abs(pow(SRT, 4)/(4 * pow(kProtonMass, 2)) - pow(SRT, 2)));
  SelNN = 11.9 + 26.9 * pow(XIMPULS, -1.21) + 0.169 * pow(log(XIMPULS), 2) - 1.85 * log(XIMPULS);

  // Inelastic X-section:
  if(SRT > 20.0){
    // part 2
    double SS  = pow(SRT, 2);
    double SS0 = pow((2.0*kProtonMass), 2);
    double SS1 = 1.0; // GeV/c2
    // parameters for pp
    double Zab  = 37.0;
    double BB   = 0.3152;
    double Y1ab = 42.1;
    double Y2ab = 32.19;
    double ETA1 = 0.533;
    double ETA2 = 0.46;
           SS0  = 34.;
    double ETA0 = 1.96;

    SigTot = Zab + BB * pow((log(SS/SS0)), ETA0) + Y1ab * pow((SS1/SS), ETA1) - Y2ab * pow((SS1/SS), ETA2);
    SigTot = SigTot * (1. + 0.08/3000. * SRT);
  }

  SinelNN = SigTot - SelNN;
}
